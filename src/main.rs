extern crate glutin;
extern crate glutin_wgl_sys;
extern crate kernel32;
extern crate user32;
extern crate widestring;
extern crate winapi;

mod gl {
    include!(concat!("", "bindings.rs"));
}

use std::ffi::c_void;
use std::ffi::CString;
use std::io::stdin;
use widestring::U16String;

unsafe extern "system" fn cbFunc(
    hWnd: winapi::shared::windef::HWND,
    Msg: winapi::shared::minwindef::UINT,
    wParam: winapi::shared::minwindef::WPARAM,
    lParam: winapi::shared::minwindef::LPARAM,
) -> winapi::shared::minwindef::LRESULT {
    match Msg {
        winapi::um::winuser::WM_CLOSE => {
            user32::PostQuitMessage(0);
        }

        winapi::um::winuser::WM_DESTROY => {
            user32::PostQuitMessage(0);
        }
        _ => return winapi::um::winuser::DefWindowProcW(hWnd, Msg, wParam, lParam),
    }

    return winapi::um::winuser::DefWindowProcW(hWnd, Msg, wParam, lParam);
}

unsafe extern "system" fn loadGLFuncPtr(symbol: &'static str) -> *const std::os::raw::c_void {
    //    let sym = CString::new(symbol).unwrap(); //.as_ptr() as *const i8;

    let sym = symbol.as_ptr() as *const i8;
    let mut err = 0;
    winapi::um::errhandlingapi::SetLastError(0);
    let ret = glutin_wgl_sys::wgl::GetProcAddress(sym) as *const std::os::raw::c_void;
    err = winapi::um::errhandlingapi::GetLastError();

    return ret;
}

fn main() {
    let assWindowName = "assWindow\0";
    let assWindowTitle = "assTitle\0";
    let assWindowStr = U16String::from_str(assWindowName);
    let assWindowTitleStr = U16String::from_str(assWindowTitle);

    unsafe {
        //        let hInstance = kernel32::GetModuleHandleW(std::ptr::null_mut());
        let hInstance = winapi::um::libloaderapi::GetModuleHandleW(std::ptr::null_mut());
        let wndClass = winapi::um::winuser::WNDCLASSW {
            style: winapi::um::winuser::CS_OWNDC
                | winapi::um::winuser::CS_HREDRAW
                | winapi::um::winuser::CS_VREDRAW,
            lpfnWndProc: Some(cbFunc),
            hInstance: hInstance,
            lpszClassName: assWindowStr.as_ptr(),
            cbClsExtra: 0,
            cbWndExtra: 0,
            hIcon: std::ptr::null_mut(),
            hCursor: std::ptr::null_mut(),
            hbrBackground: std::ptr::null_mut(),
            lpszMenuName: std::ptr::null_mut(),
        };

        let mut err = 0;
        winapi::um::errhandlingapi::SetLastError(0);
        winapi::um::winuser::RegisterClassW(&wndClass);
        err = winapi::um::errhandlingapi::GetLastError();

        err = 0;
        winapi::um::errhandlingapi::SetLastError(0);
        let hWnd = winapi::um::winuser::CreateWindowExW(
            0,
            assWindowStr.as_ptr(),
            assWindowTitleStr.as_ptr(),
            winapi::um::winuser::WS_OVERLAPPEDWINDOW | winapi::um::winuser::WS_VISIBLE,
            winapi::um::winuser::CW_USEDEFAULT,
            winapi::um::winuser::CW_USEDEFAULT,
            winapi::um::winuser::CW_USEDEFAULT,
            winapi::um::winuser::CW_USEDEFAULT,
            std::ptr::null_mut(),
            std::ptr::null_mut(),
            hInstance,
            std::ptr::null_mut(),
        );
        err = winapi::um::errhandlingapi::GetLastError();

        // static PIXELFORMATDESCRIPTOR pix_format_desc =
        // {
        //     sizeof(PIXELFORMATDESCRIPTOR), 1, PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, PFD_TYPE_RGBA,
        //     32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 8, 0, PFD_MAIN_PLANE, 0, 0, 0, 0
        // };

        // if (!(pixFormat = ChoosePixelFormat(mHDC, &pix_format_desc)))
        //     return;

        // if (!SetPixelFormat(mHDC, pixFormat, &pix_format_desc))
        //     return;

        //       let mHDC = user32::GetDC(hWnd);// as *mut c_void;
        err = 0;
        winapi::um::errhandlingapi::SetLastError(0);
        let mHDC = winapi::um::winuser::GetDC(hWnd);
        err = winapi::um::errhandlingapi::GetLastError();

        let mHDCRaw = mHDC as *mut c_void;

        let pixelFormatDesc = winapi::um::wingdi::PIXELFORMATDESCRIPTOR {
            nSize: std::mem::size_of::<winapi::um::wingdi::PIXELFORMATDESCRIPTOR>() as u16, // 40
            nVersion: 1,
            dwFlags: winapi::um::wingdi::PFD_DRAW_TO_WINDOW
                | winapi::um::wingdi::PFD_SUPPORT_OPENGL
                | winapi::um::wingdi::PFD_DOUBLEBUFFER,
            iPixelType: winapi::um::wingdi::PFD_TYPE_RGBA,
            cColorBits: 32,
            cRedBits: 0,
            cRedShift: 0,
            cGreenBits: 0,
            cGreenShift: 0,
            cBlueBits: 0,
            cBlueShift: 0,
            cAlphaBits: 0,
            cAlphaShift: 0,
            cAccumBits: 0,
            cAccumRedBits: 0,
            cAccumGreenBits: 0,
            cAccumBlueBits: 0,
            cAccumAlphaBits: 0,
            cDepthBits: 24,
            cStencilBits: 8,
            cAuxBuffers: 0,
            iLayerType: winapi::um::wingdi::PFD_MAIN_PLANE,
            bReserved: 0,
            dwLayerMask: 0,
            dwVisibleMask: 0,
            dwDamageMask: 0,
        };

        err = 0;
        winapi::um::errhandlingapi::SetLastError(0);
        let pixFormat = winapi::um::wingdi::ChoosePixelFormat(mHDC, &pixelFormatDesc);
        err = winapi::um::errhandlingapi::GetLastError();

        err = 0;
        winapi::um::errhandlingapi::SetLastError(0);
        let ret = winapi::um::wingdi::SetPixelFormat(mHDC, pixFormat, &pixelFormatDesc);
        err = winapi::um::errhandlingapi::GetLastError();

        err = 0;
        winapi::um::errhandlingapi::SetLastError(0);
        let hGLRC = glutin_wgl_sys::wgl::CreateContext(mHDCRaw);
        err = winapi::um::errhandlingapi::GetLastError();

        err = 0;
        winapi::um::errhandlingapi::SetLastError(0);
        let cur = glutin_wgl_sys::wgl::MakeCurrent(mHDCRaw, hGLRC);
        err = winapi::um::errhandlingapi::GetLastError();

        gl::ClearColor::load_with(|s| loadGLFuncPtr(s));

        let mut ext = CString::new("glClearColor");
        let mut sym = ext.unwrap();
        err = 0;
        winapi::um::errhandlingapi::SetLastError(0);
        let mut balls = glutin_wgl_sys::wgl::GetProcAddress(sym.as_ptr());
        err = winapi::um::errhandlingapi::GetLastError();

        err = 0;
        winapi::um::errhandlingapi::SetLastError(0);
        ext = CString::new("glTexStorage3D");
        err = winapi::um::errhandlingapi::GetLastError();
        sym = ext.unwrap();
        balls = glutin_wgl_sys::wgl::GetProcAddress(sym.as_ptr());

        let mut done = false;
        while !done {
            let mut msg: winapi::um::winuser::PMSG = std::mem::uninitialized();
            let ret = winapi::um::winuser::PeekMessageW(
                msg,
                std::ptr::null_mut(),
                0,
                0,
                winapi::um::winuser::PM_REMOVE,
            );

            if ret > 0 {
                if (*msg).message == winapi::um::winuser::WM_QUIT
                    || (*msg).message == winapi::um::winuser::WM_DESTROY
                {
                    done = true;
                }

                winapi::um::winuser::TranslateMessage(msg);
                winapi::um::winuser::DispatchMessageW(msg);
            }

            gl::ClearColor(1.0, 0.0, 0.0, 0.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }
    }
}
